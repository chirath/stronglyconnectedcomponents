package com.progcraft.logic;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.progcraft.model.Graph;
import com.progcraft.model.Vertex;

public class GraphReaderTest {

	private static final String TEST1 = "test1";
	private GraphReader target;
	
	@Before
	public void setUp() {
		target = new GraphReader();
	}
	
	@Test
	public void test() throws IOException {
		Graph result = target.read(TEST1);
		Vertex v1 = result.getVertex(1);
		assertArrayEquals(new Integer[]{4, 3}, v1.getHeadVertices().toArray());
		assertArrayEquals(new Integer[]{2}, v1.getTailVertices().toArray());
	}

}
