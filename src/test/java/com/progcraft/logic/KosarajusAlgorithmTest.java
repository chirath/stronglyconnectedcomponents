package com.progcraft.logic;

import static org.junit.Assert.assertArrayEquals;

import java.util.Map;

import org.junit.Test;

import com.progcraft.model.Graph;

public class KosarajusAlgorithmTest {

	@Test
	public void searchTestWithTwoOneSCC() {
		Graph graph = buildGraph(new int[]{1, 2, 2, 4, 4, 3, 3, 1, 4, 5, 5, 7, 7, 6, 6, 4});
		Map<Integer, Integer> result = KosarajusAlgorithm.run(graph);
		assertArrayEquals(new Integer[]{7}, result.values().toArray());
	}

	@Test
	public void searchTestWithTwoSCC() {
		Graph graph = buildGraph(new int[]{1, 2, 2, 3, 3, 1, 4, 2, 4, 5, 5, 6, 6, 4});
		Map<Integer, Integer> result = KosarajusAlgorithm.run(graph);
		assertArrayEquals(new Integer[]{3, 3}, result.values().toArray());
	}
	
	@Test
	public void searchTestWithThree() {
		Graph graph = buildGraph(new int[]{1, 2, 2, 3, 3, 1, 4, 2, 4, 5, 5, 6, 6, 4, 7, 2, 7, 9, 8, 7, 10, 8, 9, 10, 9, 8});
		Map<Integer, Integer> result = KosarajusAlgorithm.run(graph);
		assertArrayEquals(new Integer[]{3, 3, 4}, result.values().toArray());
	}
	
	@Test
	public void searchTestWithAllSCCConsistOfOneVertex() {
		Graph graph = buildGraph(new int[]{1, 4, 2, 8, 3, 6, 4, 7, 5, 2});
		Map<Integer, Integer> result = KosarajusAlgorithm.run(graph);
		assertArrayEquals(new Integer[]{1, 1, 1, 1, 1, 1, 1, 1}, result.values().toArray());
	}
	
	private Graph buildGraph(int[] vertecies) {
		Graph graph = new Graph();
		for(int i = 0; i < vertecies.length; i += 2) {
			graph.addEdge(vertecies[i], vertecies[i + 1]);
		}
		return graph;
	}
}
