package com.progcraft.logic.dfs;

import com.progcraft.model.Graph;
import com.progcraft.model.Vertex;

public class ReversedDepthFirstSearch extends DepthFirstSearch {

	public ReversedDepthFirstSearch(Graph graph) {
		super(graph);
	}

	@Override
	protected Integer getNextVertexNumberToVisit(Vertex vertex) {
		return vertex.getTailVertices().stream()
				.filter(currentVertex -> !visitedVertecies.contains(currentVertex))
				.findFirst().orElse(null);
	}
}
