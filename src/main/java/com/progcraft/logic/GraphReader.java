package com.progcraft.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.progcraft.model.Graph;

public class GraphReader {
	
	private static final String SEPARATOR = " ";
	
	private Graph graph;
	
	public Graph read(String fileName) throws IOException {
		graph = new Graph();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			br.lines().forEach(line -> {
				String[] values = line.split(SEPARATOR);
				graph.addEdge(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
			});
		}
		return graph;
	}
	
}
