package com.progcraft.model;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

	private int vertexNumber;
	private List<Integer>  headVertices;
	private List<Integer> tailVertices;
	
	public Vertex(Integer vertexNumber) {
		this.vertexNumber = vertexNumber;
		this.headVertices = new ArrayList<>();
		this.tailVertices = new ArrayList<>();
	}
	
	public boolean hasOutgoingEdge() {
		return !headVertices.isEmpty();
	}
	
	public void addHeadVertex(Integer vertexNumber) {
		headVertices.add(vertexNumber);
	}
	
	public void addTailVertex(Integer vertexNumber) {
		tailVertices.add(vertexNumber);
	}
	
	public int getVertexNumber() {
		return vertexNumber;
	}
	public void setVertexNumber(int vertexNumber) {
		this.vertexNumber = vertexNumber;
	}
	public List<Integer> getHeadVertices() {
		return headVertices;
	}
	public void setHeadVertices(List<Integer> headVertices) {
		this.headVertices = headVertices;
	}
	public List<Integer> getTailVertices() {
		return tailVertices;
	}
	public void setTailVertices(List<Integer> tailVertices) {
		this.tailVertices = tailVertices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((headVertices == null) ? 0 : headVertices.hashCode());
		result = prime * result + ((tailVertices == null) ? 0 : tailVertices.hashCode());
		result = prime * result + vertexNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (headVertices == null) {
			if (other.headVertices != null)
				return false;
		} else if (!headVertices.equals(other.headVertices))
			return false;
		if (tailVertices == null) {
			if (other.tailVertices != null)
				return false;
		} else if (!tailVertices.equals(other.tailVertices))
			return false;
		if (vertexNumber != other.vertexNumber)
			return false;
		return true;
	}
}
